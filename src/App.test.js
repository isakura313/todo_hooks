import { render, screen } from '@testing-library/react';
import App from './App';

test('показываем что отдается самое первое дело', () => {
  render(<App />);
  const linkElement = screen.getByText(/Star Wars/i);
  expect(linkElement).toBeInTheDocument();
});
